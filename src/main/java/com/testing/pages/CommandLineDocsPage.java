package com.testing.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testing.base.BasePage;

public class CommandLineDocsPage extends BasePage {

	// XPath
	@FindBy(linkText = "Command Line Docs")
	WebElement commandLineDocsElement;
	
	@FindBy(xpath = "//*[@class='css-1dbjc4n']/img")
	WebElement toptalLogoImageElement;

	// Initializing the Page Objects:
	public CommandLineDocsPage() {

		PageFactory.initElements(driver, this);
		commandLineDocsElement.click();
	}

	public boolean verifyToptalImage() {

		return toptalLogoImageElement.isDisplayed();

	}

	public String validateTitle() {
		return driver.getTitle();
	}

}

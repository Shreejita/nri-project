package com.testing.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testing.base.BasePage;

public class SourceCodePage extends BasePage {

	// XPath
	@FindBy(linkText = "Source Code")
	WebElement sourceCodeLink;

	@FindBy(xpath = "//*[@id='repository-container-header']//h1//a")
	WebElement gitRepoNameElement;

	// Initializing the Page Objects:
	public SourceCodePage() {

		PageFactory.initElements(driver, this);
		sourceCodeLink.click();
	}

	public String verifyLandingRepo() {

		return gitRepoNameElement.getText();

	}
	
	public String validateTitle()
	{
		return driver.getTitle();
	}

}

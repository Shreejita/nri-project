package com.testing.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testing.base.BasePage;

public class ToptalPage extends BasePage {

	// XPath
	@FindBy(xpath = "//*[@class='header-left-column']//img")
	WebElement toptalImageElement;

	@FindBy(className = "_1PHP5q1Y")
	WebElement heroTitleElement;

	// Initializing the Page Objects:
	public ToptalPage() {

		PageFactory.initElements(driver, this);
		toptalImageElement.click();
	}

	public String verifyToptalPageTitle() {

		return driver.getTitle();
	}

	public String verifyHeroTitle() {
		return heroTitleElement.getText();
	}

}

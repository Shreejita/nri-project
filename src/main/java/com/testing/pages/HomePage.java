package com.testing.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testing.base.BasePage;

public class HomePage extends BasePage {

	// XPath
	@FindBy(xpath = "//*[@class='masthead-top']/img")
	WebElement gitignoreImageElement;
	
	@FindBy(xpath = "//*[@class='masthead-top']/h2")
	WebElement descriptionElement;
	
	// Initializing the Page Objects:
	public HomePage() {

		PageFactory.initElements(driver, this);
	}

	public String validateLoginPageTitle() {
		return driver.getTitle();
	}

	public boolean validateGitignoreImage() {
		return gitignoreImageElement.isDisplayed();
	}
	
	public String validateDescription()
	{
		return descriptionElement.getText();
	}
	
	public List<String> actualFooterLinks()
	{
		List<WebElement> actualLinks = driver.findElements(By.xpath("//*[@class='internal-linking__ticker-list']/a"));
		List<String> actualLinkList = new ArrayList<>();

		for (WebElement a : actualLinks.subList(0, 6)) {
			actualLinkList.add(a.getText());
		}
		
		return actualLinkList;
	}
	
	public List<String> expectedFooterLinks()
	{
		List<String> expectedFooterLinkst = new ArrayList<>();
		expectedFooterLinkst.add("React.js Developer Jobs");
		expectedFooterLinkst.add("Node.js Developer Jobs");
		expectedFooterLinkst.add("Ruby on Rails Developer Jobs");
		expectedFooterLinkst.add("Azure Developer Jobs");
		expectedFooterLinkst.add("React Native Developer Jobs");
		expectedFooterLinkst.add("QA Engineer Jobs");
		
		return expectedFooterLinkst;
	}
	

}

package com.testing.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testing.base.BasePage;

public class SearchBoxPage extends BasePage {

	// XPath
	@FindBy(className = "select2-search__field")
	WebElement searchBoxElement;

	@FindBy(xpath = "//*[@class='select2-results__options']/li")
	WebElement searchOptions;

	@FindBy(id = "btn-gitignore")
	WebElement clickButtonElement;

	String randomString;
	RecordResult recordResult = null;
	List<String> actualResultOptionsList;

	// Initializing the Page Objects:
	public SearchBoxPage() {

		PageFactory.initElements(driver, this);
	}

	public RecordResult getRecordedResult() {
		return recordResult;
	}

	public List<String> actualResultOptions() {
		searchBoxElement.sendKeys("test");
		List<WebElement> actualResultOptions = driver.findElements(By.xpath("//*[@class='select2-results']//li"));

		actualResultOptionsList = new ArrayList<>();

		for (WebElement a : actualResultOptions) {
			actualResultOptionsList.add(a.getText());
		}

		return actualResultOptionsList;
	}

	public List<String> expectedResultOptions() {
		List<String> expectedResultOptions = new ArrayList<>();
		expectedResultOptions.add("Test");
		expectedResultOptions.add("Testinfra");
		expectedResultOptions.add("TestComplete");
		
		return expectedResultOptions;
	}

	public String validateUrlOfProject() throws InterruptedException {

		Random generator = new Random();
		randomString = actualResultOptionsList.get(generator.nextInt(actualResultOptionsList.size()));

		String attributeValue = searchOptions.findElement(By.xpath("//li[contains(text(),'" + randomString + "')]")).getAttribute("id");

		driver.findElement(By.id(attributeValue)).click();
		
		// searchBoxElement.click();
		clickButtonElement.click();
		Thread.sleep(2000);

		recordResult = RecordResult.getInstance();
		recordResult.setExpectedURL("https://www.toptal.com/developers/gitignore/api/" + randomString.toLowerCase());

		return driver.getCurrentUrl();

	}

	public static class RecordResult {

		private static RecordResult result;
		private String expectedURL = null;

		public static RecordResult getInstance() {
			if (result == null) {
				result = new RecordResult();
			}
			return result;
		}

		private void setExpectedURL(String expectedURL) {
			this.expectedURL = expectedURL;
		}

		public String getExpectedURL() {
			return expectedURL;
		}
	}
}

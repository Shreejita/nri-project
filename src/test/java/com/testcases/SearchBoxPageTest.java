package com.testcases;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.testing.base.BasePage;
import com.testing.pages.SearchBoxPage;

public class SearchBoxPageTest extends BasePage {

	SearchBoxPage searchBoxPage;

	public SearchBoxPageTest() {
		super();
	}

	@BeforeClass
	public void setUp() {
		init();
		searchBoxPage = new SearchBoxPage();
	}

	@Test(priority = 1, description = "Verifying Seach Result")
	public void verifySeachResult() {

		List<String> actualSearchResult = searchBoxPage.actualResultOptions();
		List<String> expectedSearchResult = searchBoxPage.expectedResultOptions();

		Assert.assertEquals(actualSearchResult, expectedSearchResult);

	}

	@Test(priority = 2, description = "Validate Project url")
	public void validateProjectURL() throws InterruptedException {
		String actualURL = searchBoxPage.validateUrlOfProject();
		String expectedURL = searchBoxPage.getRecordedResult().getExpectedURL();

		Assert.assertEquals(actualURL, expectedURL);
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}

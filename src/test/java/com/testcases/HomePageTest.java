package com.testcases;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.testing.base.BasePage;
import com.testing.pages.HomePage;

public class HomePageTest extends BasePage {

	HomePage homePage;
	
	public HomePageTest() {
		super();
	}

	@BeforeClass
	public void setUp() {
		init();
		homePage = new HomePage();
	}

	@Test(description = "Verifying website tile")
	public void verifyTitle() {
		String title = homePage.validateLoginPageTitle();
		Assert.assertEquals("gitignore.io - Create Useful .gitignore Files For Your Project" , title);
	}

	@Test(description = "Verify Image is present")
	public void verifyImage() {
		boolean flag = homePage.validateGitignoreImage();
		Assert.assertTrue(flag);
	}

	@Test(description = "Verify Description")
	public void verifyDescriptionText() {
		String description = homePage.validateDescription();
		Assert.assertEquals("Create useful .gitignore files for your project" , description);
	}
	
	@Test(description = "Verify Partial Footer Links")
	public void verifyFooterLinks() {
		List<String> actualLinkList = homePage.actualFooterLinks();
		List<String> expectedLinkList = homePage.expectedFooterLinks();
	
		Assert.assertEquals(expectedLinkList, actualLinkList);
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}

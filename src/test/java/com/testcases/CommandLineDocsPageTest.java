package com.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.testing.base.BasePage;
import com.testing.pages.CommandLineDocsPage;

public class CommandLineDocsPageTest extends BasePage {

	CommandLineDocsPage commandLineDocsPage;

	public CommandLineDocsPageTest() {
		super();
	}

	@BeforeClass
	public void setUp() {
		init();
		commandLineDocsPage = new CommandLineDocsPage();
	}

	@Test(description = "Verifying Toptal Logo is present")
	public void verifyToptalLogoIsPresent() {

		boolean flag = commandLineDocsPage.verifyToptalImage();
		Assert.assertTrue(flag);
	}

	@Test(description = "Verify Command Line Docs Page Title")
	public void verifyGitLabTitle() {
		Assert.assertEquals(commandLineDocsPage.validateTitle(), "Command Line - gitignore.io / docs");
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}

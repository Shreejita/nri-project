package com.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.testing.base.BasePage;
import com.testing.pages.ToptalPage;

public class ToptalPageTest extends BasePage {

	ToptalPage toptalPage;

	public ToptalPageTest() {
		super();
	}

	@BeforeClass
	public void setUp() {
		init();
		toptalPage = new ToptalPage();
	}

	@Test(description = "Verifying Toptal page tile")
	public void verifyToptalPageTitle() {
		String title = toptalPage.verifyToptalPageTitle();
		Assert.assertEquals("Hire Freelance Developers from the Top 3% - Toptal®", title);
	}

	@Test(description = "Verify Totptal page Hero description")
	public void verifyToptalPageDescription() {
		String actualDescription = toptalPage.verifyHeroTitle();
		Assert.assertEquals("Hire the Top 3% of Developers", actualDescription);
	}
	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}

package com.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.testing.base.BasePage;
import com.testing.pages.SourceCodePage;

public class SourceCodePageTest extends BasePage {

	SourceCodePage sourceCodePage;

	public SourceCodePageTest() {
		super();
	}

	@BeforeClass
	public void setUp() {
		init();
		sourceCodePage = new SourceCodePage();
	}

	@Test(description = "Verifying Landing Git repo")
	public void verifySeachResult() {

		Assert.assertEquals(sourceCodePage.verifyLandingRepo(), "toptal");

	}
	
	@Test(description = "Verify Gitlab Title")
	public void verifyGitLabTitle()
	{
		Assert.assertEquals(sourceCodePage.validateTitle(), "GitHub - toptal/gitignore.io: Create useful .gitignore files for your project");
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
